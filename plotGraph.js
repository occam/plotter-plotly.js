// When true, plotly is fed data in the global "plotlyJSON"
// instead of plotlyJSON being generated
var nativeJSON = false;

var plotlyJSON = {};

var graph = {
  // General configuration options
  layout: {
    title: "Program Performance",
    titleColor: "#ccc",

    backgroundColor: "#444",
    textColor: "#ccc",

    showlegend: true,
    margin: {
      l: 80,
      r: 80,
      t: 100,
      b: 80,
      pad: 80
    },

    xaxis: {
      // The overall label for the axis
      title: "Application",
      type: "detect",

      // The color of the axis
      tickColor: "#333",
      minorTickColor: "#383838",
      textColor: "#ccc",

      // The label for each value
      series: [
        {
          name: "fft"
        },
        {
          name: "mmult"
        },
        {
          name: "tar"
        },
        {
          name: "blob"
        }
      ],

      showline: true,
      linecolor: "#222",
      linewidth: 1,

      zeroline: true,
      zerolinecolor: "#333",
      zerolinewidth: 1,

      range: {
        autorange: true,
        rangemode: 'normal',
        fixedrange: false,
        start: 0,
        end: 0
      },

      grid: {
        showgrid: true,
        gridcolor: "#333",
        gridwidth: 1
      },
    },
    yaxis: {
      // The color of the axis
      color: "#222",
      tickColor: "#333",
      minorTickColor: "#3d3d3d",
      textColor: "#ccc",
      type: "linear",

      // Tick steps
      majorStep: 10, // Major steps get a text label
      minorStep: 2,  // Minor steps get just a tick

      // The overall label for the axis
      title: "% of cpu",

      showline: true,
      linecolor: "#222",
      linewidth: 1,

      zeroline: true,
      zerolinecolor: "#333",
      zerolinewidth: 3,

      range: {
        autorange: true,
        rangemode: 'normal',
        fixedrange: false,
        start: 0,
        end: 0
      },

      grid: {
        showgrid: true,
        gridcolor: "#333",
        gridwidth: 1
      },
    }
  },
  data: {
    groups: [{
      // Name for this group
      name:        "gcc",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 0,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    },
    {
      // Name for this group
      name:        "clang",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 1,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    },
    {
      // Name for this group
      name:        "clang",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 2,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    },
    {
      // Name for this group
      name:        "clang",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 3,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    },
    {
      // Name for this group
      name:        "clang",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 4,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    },
    {
      // Name for this group
      name:        "clang",

      // Type for group
      type: 		   "scatter",

      // Color for this group
      color:       "hsl(120, 60%, 60%)",

      // Visible 	true  |  false  |  "legendonly"
      visible:	 true,

      // Boolean
      showlegend:	 true,

      // Legend group for the legend
      legendgroup: "",

      // Opacity of the element	0-1
      opacity: 1,

      // Text to display
      text: "",

      //  any combination of "lines" "markers" "text" joined with "+" or "none"
      mode: "",

      //	Boolean; if gaps ({nan} or missing values) in data arrays are connected
      connectgaps: true,

      //	line
      line:{
        width: 3,
        //	"linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
        shape: "spline",
        //  only for "spline", 0-1.3
        smoothing: 1,
        //  String, default "solid", set to dash type or pixel length
        dash: 5,
        //	markers
        color: ""
      },

      marker: {
        symbol: "circle",
        opacity: 1,
        size: 8,
        maxdisplayed: 0,
        color: "",
      },

      filloptions: {
        fillmode: 'none',
        fillcolor: "#ccc"
      },

      bar: {
        orientation: 'v'
      },
    }]
  }
};

//		Here's the data
var data = {
  data: {
    groups: [{
      y: {
        values: [20, 40, 90, 75],
        units: "GB/s"
      }
    }, {
      y: {
        values: [73, 32, 76, 70],
        units: "GB/s"
      }
    }, {
      y: {
        values: [13, 64, 64, 26],
        units: "GB/s"
      }
    }, {
      y: {
        values: [22, 52, 70, 18],
        units: "GB/s"
      }
    }, {
      y: {
        values: [34, 36, 50, 10],
        units: "GB/s"
      }
    }, {
      y: {
        values: [60, 31, 49, 60],
        units: "GB/s"
      }
    }]
  }
};

function setData(description, data){
  var dataArray = [];

	// Look at configuration parameters
  var layout = description.layout;
  var groups = description.data.groups;

  // Get the data dimensions
  if (!layout) {
    return;
  }
  if (!groups) {
    return;
  }
  if (!(data && data.data)) {
    return;
  }

  data = data.data.groups;
  var n = data[0].y.values.length;	// 4 elements of Y in each object
  var m = data.length;		// 6 data objects to make

  var xLabels = layout.xaxis.series || [];

  if (xLabels === undefined) {
    xLabels = [];
  }

  // Ensure defaults for X Labels
  if (xLabels.length < n) {
    xLabels = xLabels.concat(d3.range(xLabels.length, n).map(function(e) {
      return {"name": e};
    }));
  }

  var x = [];

  xLabels.forEach(function(k, i) {
    x.push(i);
  });

  for(var i = 0; i < m; i++){
    // Hmm. The group is missing, but the data values exist
    if (i >= groups.length) {
      continue;
    }

	  var dataElement = {};

	  dataElement.x           = x;                     // Assigning X values to one data object
	  dataElement.y           = data[i].y.values;      // Assigning Y values

	  dataElement.type        = groups[i].type;        // "scatter" - line; "bar" - bar
	  dataElement.name        = groups[i].name;	       // line and legend name
	  dataElement.visible     = groups[i].visible;     //	true  |  false  |  "legendonly"
	  dataElement.showlegend  = groups[i].showlegend;  // Boolean
	  dataElement.legendgroup = groups[i].legendgroup; // String
	  dataElement.opacity     = groups[i].opacity;     // 0-1
	  dataElement.text        = groups[i].text;        // text to display on data element
	  dataElement.mode        = groups[i].mode;        // any combination of "lines" "markers" "text" joined with "+" or "none"

	  dataElement.stream           = {};
	  dataElement.stream.token     = "id"; // id number links data trace with stream
	  dataElement.stream.maxpoints = 50;   // max points to display

	  // Line Options
    dataElement.line = {};
    dataElement.line.color     = groups[i].line.color;       // line color
    dataElement.line.width     = groups[i].line.width;       // Line stroke width
    dataElement.line.shape     = groups[i].line.shape;       // "linear" | "spline" | "hv" | "vh" | "hvh" | "vhv"
    dataElement.line.smoothing = groups[i].line.smoothing;   // only for "spline", 0-1.3
    dataElement.line.dash      = groups[i].line.dash;			   // String, default "solid", set to dash type or pixel length

    // Reinterpret Line Dash
    if (dataElement.line.dash == 0) {
      dataElement.line.dash = "solid";
    }

    dataElement.connectgaps    = groups[i].connectgaps;      // Boolean; if gaps ({nan} or missing values) in data arrays are connected

    // Marker Options
    dataElement.marker = {};
    dataElement.marker.symbol       = groups[i].marker.symbol;
    dataElement.marker.opacity      = groups[i].marker.opacity;
    dataElement.marker.size         = groups[i].marker.size;
    dataElement.marker.color        = groups[i].marker.color;
    dataElement.marker.maxdisplayed = groups[i].marker.maxdisplayed;

    dataElement.marker.line = {};
    dataElement.marker.line.color = groups[i].marker.borderColor;
    dataElement.marker.line.width = groups[i].marker.borderWidth;

    dataElement.fill      = groups[i].filloptions.fillmode;
    dataElement.fillcolor = groups[i].filloptions.fillcolor;

	  // Bar Options
    dataElement.orientation = groups[i].bar.orientation; // "h" | "v"  orientation of the bars

    if(dataElement.type == "heatmap"){
      var z = [2];
      var test = [20,40,75,90];
      var test2 = [50,65,85,100];
      z[0] = test;
      z[1] = test2;
      dataElement.z = z;
	  }
    else if(dataElement.type == "histogram"){
	  }
    else if(dataElement.type == "histogram2d"){
	  }
    else if(dataElement.type== "pie"){
	  }
    else if(dataElement.type == "contour"){
      var z = [2];
      var test = [20,40,75,90];
      var test2 = [50,65,85,100];
      z[0] = test;
      z[1] = test2;
      dataElement.z = z;
	  }
    else if(dataElement.type == "histogram2dcontour"){
	  }
    else if(dataElement.type == "scatter3d"){
      dataElement.x = [35,44,57,70];
      var z = [10,25,50,75];
      //var test = [10,25,50,75];
      //var test2 = [50,65,85,100];
      //z[0] = test;
      //z[1] = test2;
      dataElement.z = z;
	  }
    else if(dataElement.type == "surface"){
	  }
    else if(dataElement.type == "mesh3d"){
	  }
    else if(dataElement.type == "scattergeo"){
	  }
    else if(dataElement.type == "choropleth"){
	  }
    else if(dataElement.type == "scattergl"){
	  }
    else if(dataElement.type == "area"){
	  }
    else if(dataElement.type == "layout"){
	  }

	  dataArray.push(dataElement);	// Adding data object to the array
  }

  return dataArray;
}

function setLayout(description, data){
	// Look at configuration parameters
  var layout = description.layout;

  // Set background color
  layout.paper_bgcolor = layout.backgroundColor;
  layout.plot_bgcolor  = layout.backgroundColor;
  d3.select('body').style({
    "background-color": layout.backgroundColor
  });

  layout.font = {};
  layout.font.color = layout.textColor;

  // Tick Colors
  layout.xaxis.tickcolor = layout.xaxis.tickColor;
  layout.xaxis.tickfont = {}
  layout.xaxis.tickfont.color = layout.xaxis.textColor;

  layout.yaxis.tickcolor = layout.yaxis.tickColor;
  layout.yaxis.tickfont = {}
  layout.yaxis.tickfont.color = layout.yaxis.textColor;

  // Tick Names
  if (!(data && data.data)) {
    return;
  }

  data = data.data.groups;
  var n = data[0].y.values.length;
  var xLabels = layout.xaxis.series || [];

  if (xLabels.length > 0) {
    // Ensure defaults for X Labels
    if (xLabels.length < n) {
      xLabels = xLabels.concat(d3.range(xLabels.length, n).map(function(e) {
        return {"name": e};
      }));
    }

    // Categorical Data:
    layout.xaxis.tickmode = "array";
    layout.xaxis.tickvals = d3.range(0, n);
    layout.xaxis.ticktext = xLabels.map(function(e) { return e.name; });
  }

  // Grid Options
  layout.xaxis.showgrid  = layout.xaxis.grid.showgrid;
  layout.xaxis.gridwidth = layout.xaxis.grid.gridwidth;
  layout.xaxis.gridcolor = layout.xaxis.grid.gridcolor;
  layout.yaxis.showgrid  = layout.yaxis.grid.showgrid;
  layout.yaxis.gridwidth = layout.yaxis.grid.gridwidth;
  layout.yaxis.gridcolor = layout.yaxis.grid.gridcolor;

  // Set graph title
  layout.titlefont = {};
  layout.titlefont.color = layout.titleColor;

  // Set autosize?
  //layout.autosize = true;

  // Set X axis attributes
  layout.xaxis.titlefont = {};
  layout.xaxis.titlefont.color = layout.xaxis.textColor;

  // Set Y axis attributes
  layout.yaxis.titlefont = {};
  layout.yaxis.titlefont.color = layout.yaxis.textColor;

  // Set Graph Size
  layout.width  = description.width;
  layout.height = description.height;

  return layout;
}

function init(description, data) {
  var myGraph = document.getElementById('myGraph');

  var plotData   = setData(description, data);
  var plotLayout = setLayout(description, data);

  Plotly.plot( myGraph, plotData, plotLayout );

  return myGraph;
}

function draw(plot, description, data) {
  if (nativeJSON) {
    plot.data   = plotlyJSON.data;
    plot.layout = plotlyJSON.layout;

    Plotly.relayout(plot, {
      "width":  graph.width,
      "height": graph.height
    });
  }
  else {
    plot.data   = setData(description, data);
    plot.layout = setLayout(description, data);
  }

  Plotly.redraw(plot);
}

function resize(plot) {
  Plotly.relayout(plot, {
    "width":  graph.width,
    "height": graph.height
  });
}

var datapointCache = {
  groups: []
};

// Initialize the graph
init(graph, data);

// If the window resizes, redraw the graph to fit
window.onresize = function(event) {
  graph.width  = window.innerWidth  - 20;
  graph.height = window.innerHeight - 20;

  var plot = document.getElementById('myGraph');
  resize(plot);
};
window.onresize();

function loadJSON(path, success, error) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        if (success)
          success(JSON.parse(xhr.responseText));
      } else {
        if (error) {
          error(xhr);
        }
      }
    }
  };
  xhr.open("GET", path, true);
  xhr.send();
}

// Handle OCCAM events
window.addEventListener('message', function(event) {
  var plot = document.getElementById('myGraph');

  if (event.data.name === 'updateConfiguration') {
    graph = event.data.data;

    graph.width  = window.innerWidth  - 20;
    graph.height = window.innerHeight - 20;

    /* Send a request to resolve the datapoints, if need be */

    // Determine if graph data is out of date or missing
    var stale = false;
    for (var i = 0; i < graph.data.groups.length; i++) {
      if (i >= datapointCache.groups.length || i >= data.data.groups.length) {
        stale = true;
        break;
      }

      var test = JSON.stringify(graph.data.groups[i].y);
      if (datapointCache.groups[i].y != test) {
        stale = true;
      }
    }

    // Retrieve the graph data
    if (stale) {
      var queuedEvent = function(event) {
        window.removeEventListener('message', queuedEvent);
      };

      window.addEventListener('message', queuedEvent);

      window.parent.postMessage({
        'name': 'updateData',
        'data': graph
      }, '*');
    }

    draw(plot, graph, data);
  }
  else if (event.data.name === 'updateData') {
    data = event.data.datapoints;

    // Mark the data into the datapoint cache
    for (var i = 0; i < event.data.data.data.groups.length; i++) {
      var test = JSON.stringify(event.data.data.data.groups[i].y);
      if (i >= datapointCache.groups.length) {
        datapointCache.groups.push({y:""});
      }
      datapointCache.groups[i].y = test;
    }

    draw(plot, graph, data);
  }
  else if (event.data.name === 'updateInput') {
    var objectInfo = event.data.data;

    var filename = objectInfo.file;
    var url = "/objects/" + objectInfo.id       +
      "/" + objectInfo.revision +
      "/raw/" + filename;

    loadJSON(url, function(data) {
      plotlyJSON = data;
      nativeJSON = true;

      draw(plot);

      window.parent.postMessage({
        'name': 'updateStatus',
        'data': 'loaded'
      }, '*');
    });
  }
});

// We are ready.
// Request configuration data.
window.parent.postMessage({
  'name': 'updateConfiguration'
}, '*');

// Request input file (if any)
window.parent.postMessage({
  'name': 'updateInput'
}, '*');
