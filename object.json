{
  "id": "e307a13a-9224-11e5-82e9-001fd05bb228",
  "type": "plotter",
  "group": "graph",
  "name": "plotly.js",
  "tags": ["graph", "line", "chart", "data", "bar"],

  "file": "widget.html",

  "website": "https://plot.ly/javascript/",
  "organization": "plotly",
  "license": "MIT",
  "authors": [
    "Dr. Alex Johnson"
  ],

  "architecture": "html",

  "capabilities": ["interactive", "javascript"],

  "views": [{
    "type": "application/json",
    "subtype": "plotly"
  }],

  "configurations": [
    {
      "file": "data.json",
      "type": "application/json",
      "name": "data",
      "label": "Data",
      "schema": {
        "groups": {
          "label":    "Groups",
          "type":     "array",
          "color":    "marker.color",
          "sublabel": "name",

          "element": {
            "type": {
              "label": "Type",
              "type": ["scatter", "bar"],
              "enables": [{
                "sibling": "symbol",
                "is": "scatter"
              }]
            },

            "name": {
              "label": "Name",
              "default": "Group",
              "type": "string"
            },

            "opacity": {
              "label": "Opacity",
              "default": 1,
              "type": "float",
              "description": "The opacity of the data."
            },

            "showlegend": {
              "label": "Show in Legend",
              "type": "boolean",
              "default": true,
              "description": "Whether or not an item corresponding to this data is shown in the legend."
            },

            "legendgroup": {
              "label": "Legend Group",
              "type": "string",
              "default": "",
              "description": "The legend group for this data. Data that are part of the same legend group will hide/show at the same time when toggling legend items."
            },

            "connectgaps": {
              "label": "Connect Gaps",
              "type": "boolean",
              "default": false,
              "description": "Whether or not gaps (i.e. NaN or missing values) in the provided data points are connected."
            },

            "y": {
              "label": "Y Values",
              "type": "datapoints"
            },

            "x": {
              "label": "X Values",
              "type": "datapoints"
            },

            "marker": {
              "label": "Marker Options",

              "opacity": {
                "type": "float",
                "default": 1,
                "label": "Opacity",
                "description": "The marker opacity."
              },

              "color": {
                "type": "color",
                "default": "#ccc",
                "defaults": ["rgba(149, 131, 237, 1.0)", "rgba(136,204,238,1.0)", "rgba(68,170,153,1.0)", "rgba(17,119,51,1.0)", "rgba(153,153,51,1.0)", "rgba(221,204,119,1.0)", "rgba(204,102,119,1.0)", "rgba(136,34,85,1.0)", "rgba(170,68,153,1.0)"],
                "label": "Color",
                "description": "The marker color."
              },

              "size": {
                "type": "int",
                "default": 9,
                "label": "Size",
                "description": "The marker size (in pixels.)"
              },

              "maxdisplayed": {
                "type": "int",
                "default": 0,
                "label": "Max Displayed",
                "description": "The maximum number of points to be drawn on the graph. 0 corresponds to no limit."
              },

              "borderColor": {
                "type": "color",
                "default": "#444",
                "label": "Border Color",
                "description": "The color of the border around the marker."
              },

              "borderWidth": {
                "type": "int",
                "default": 3,
                "label": "Border Width",
                "description": "The width of the border around the marker."
              },

              "symbol": {
                "default": "circle",
                "label":   "Symbol",
                "type": ["circle",             "circle-open",             "circle-dot",             "circle-open-dot",
                         "square",             "square-open",             "square-dot",             "square-open-dot",
                         "diamond",            "diamond-open",            "diamond-dot",            "diamond-open-dot",
                         "cross",              "cross-open",              "cross-dot",              "cross-open-dot",
                         "x",                  "x-open",                  "x-dot",                  "x-open-dot",
                         "triangle-up",        "triangle-up-open",        "triangle-up-dot",        "triangle-up-open-dot",
                         "triangle-down",      "triangle-down-open",      "triangle-down-dot",      "triangle-down-open-dot",
                         "triangle-left",      "triangle-left-open",      "triangle-left-dot",      "triangle-left-open-dot",
                         "triangle-right",     "triangle-right-open",     "triangle-right-dot",     "triangle-right-open-dot",
                         "triangle-ne",        "triangle-ne-open",        "triangle-ne-dot",        "triangle-ne-open-dot",
                         "triangle-se",        "triangle-se-open",        "triangle-se-dot",        "triangle-se-open-dot",
                         "triangle-sw",        "triangle-sw-open",        "triangle-sw-dot",        "triangle-sw-open-dot",
                         "triangle-nw",        "triangle-nw-open",        "triangle-nw-dot",        "triangle-nw-open-dot",
                         "pentagon",           "pentagon-open",           "pentagon-dot",           "pentagon-open-dot",
                         "hexagon",            "hexagon-open",            "hexagon-dot",            "hexagon-open-dot",
                         "hexagon2",           "hexagon2-open",           "hexagon2-dot",           "hexagon2-open-dot",
                         "octagon",            "octagon-open",            "octagon-dot",            "octagon-open-dot",
                         "star",               "star-open",               "star-dot",               "star-open-dot",
                         "hexagram",           "hexagram-open",           "hexagram-dot",           "hexagram-open-dot",
                         "star-triangle-up",   "star-triangle-up-open",   "star-triangle-up-dot",   "star-triangle-up-open-dot",
                         "star-triangle-down", "star-triangle-down-open", "star-triangle-down-dot", "star-triangle-down-open-dot",
                         "star-square",        "star-square-open",        "star-square-dot",        "star-square-open-dot",
                         "star-diamond",       "star-diamond-open",       "star-diamond-dot",       "star-diamond-open-dot",
                         "diamond-tall",       "diamond-tall-open",       "diamond-tall-dot",       "diamond-tall-open-dot",
                         "diamond-wide",       "diamond-wide-open",       "diamond-wide-dot",       "diamond-wide-open-dot",
                         "hourglass",          "hourglass-open",
                         "bowtie",             "bowtie-open",
                         "circle-cross",       "circle-cross-open",
                         "circle-x",           "circle-x-open",
                         "square-cross",       "square-cross-open",
                         "square-x",           "square-x-open",
                         "diamond-cross",      "diamond-cross-open",
                         "diamond-x",          "diamond-x-open",
                         "cross-thin",         "cross-thin-open",
                         "x-thin",             "x-thin-open",
                         "asterisk",           "asterisk-open",
                         "hash",               "hash-open",               "hash-dot",               "hash-open-dot",
                         "y-up",               "y-up-open",
                         "y-down",             "y-down-open",
                         "y-left",             "y-left-open",
                         "y-right",            "y-right-open",
                         "line-ew",            "line-ew-open",
                         "line-ns",            "line-ns-open",
                         "line-ne",            "line-ne-open",
                         "line-nw",            "line-nw-open"]
              }
            },

            "filloptions": {
              "label": "Fill Options",

              "fillmode": {
                "type": ["none", "tozeroy", "tozerox", "tonexty", "tonextx"],
                "default": "none",
                "label": "Fill Mode",
                "description": "How the area is filled with a solid color."
              },

              "fillcolor": {
                "type": "color",
                "default": "#ccc",
                "label": "Fill Color",
                "description": "The color to use to fill the graph."
              }
            },

            "line": {
              "label": "Line Options",

              "color": {
                "type": "color",
                "label": "Color",
                "default": "#ccc",
                "defaults": ["rgba(149, 131, 237, 0.5)", "rgba(136,204,238,0.5)", "rgba(68,170,153,0.5)", "rgba(17,119,51,0.5)", "rgba(153,153,51,0.5)", "rgba(221,204,119,0.5)", "rgba(204,102,119,0.5)", "rgba(136,34,85,0.5)", "rgba(170,68,153,0.5)"],
                "description": "The color of the lines bounding the marker points."
              },

              "width": {
                "type": "int",
                "label": "Width",
                "default": 9,
                "description": "The width (in pixels) of the lines bounding the marker points."
              },

              "shape": {
                "type": ["linear", "spline", "hv", "vh", "hvh", "vhv"],
                "default": "spline",
                "label": "Shape",
                "description": "The line shape. A 'linear' shape makes lines go directly from point to point. A 'spline' shape is drawn using spline interpolation. The smoothing is controlled in the Smoothing value. The other values correspond to step-wise line shapes."
              },

              "smoothing": {
                "type": "float",
                "label": "Smoothing",
                "default": 1,
                "description": "Only matters when shape is 'spline.' The amount of smoothing. 0 corresponds to no smoothing and is equivalent to a 'linear' shape."
              },

              "dash": {
                "label": "Dash Length",
                "type": "int",
                "default": 0,
                "description": "The length of the dashes marking the line (in pixels.) If 0, the line is solid."
              }
            },

            "bar": {
              "label": "Bar Options",

              "orientation": {
                "label": "Orientation",
                "type": ["v", "h"],
                "default": "v",
                "description": "The orientation of the bars. 'v' is vertical, 'h' is horizontal."
              }
            }
          }
        }
      }
    },
    {
      "file": "layout.json",
      "type": "application/json",
      "name": "layout",
      "label": "Layout",
      "schema": {
        "title": {
          "label": "Title",
          "default": "My Graph",
          "type": "string"
        },

        "titleColor": {
          "label": "Title Color",
          "default": "#ccc",
          "type": "color"
        },

        "backgroundColor": {
          "label": "Background Color",
          "default": "#444",
          "type": "color"
        },

        "textColor": {
          "label": "Text Color",
          "default": "#ccc",
          "type": "color"
        },

        "showlegend": {
          "type": "boolean",
          "default": true,
          "label": "Show Legend",
          "description": "Whether or not a legend is drawn."
        },

        "margin": {
          "label": "Margins",

          "l": {
            "type": "int",
            "default": 80,
            "label": "Left",
            "description": "The left margin (in pixels.)"
          },

          "r": {
            "type": "int",
            "default": 80,
            "label": "Right",
            "description": "The right margin (in pixels.)"
          },

          "t": {
            "type": "int",
            "default": 100,
            "label": "Top",
            "description": "The top margin (in pixels.)"
          },

          "b": {
            "type": "int",
            "default": 80,
            "label": "Bottom",
            "description": "The bottom margin (in pixels.)"
          },

          "pad": {
            "type": "int",
            "default": 0,
            "label": "Padding",
            "description": "The amount of padding (in pixels) between the plotting area and the axis lines."
          }
        },

        "xaxis": {
          "label": "X Axis",

          "title": {
            "label": "Name",
            "default": "",
            "type": "string"
          },

          "type": {
            "label": "Type",
            "default": "detect",
            "type": ["detect", "linear", "log", "date", "category"],
            "description": "The axis type. By default, with 'detect', it attempts to determine the axis type by looking into the data of the groups that reference the axis in question."
          },

          "showline": {
            "label": "Show Line",
            "type": "boolean",
            "default": true,
            "description": "Whether or not the bounding line for the axis is drawn."
          },

          "linecolor": {
            "label": "Line Color",
            "default": "#222",
            "type": "color",
            "description": "The color of the axis line."
          },

          "linewidth": {
            "label": "Line Width",
            "default": 1,
            "type": "int",
            "description": "The width (in pixels) of the axis line."
          },

          "zeroline": {
            "label": "Zero Line",
            "default": false,
            "type": "boolean",
            "description": "Whether or not a line is drawn at the 0 value of the axis."
          },

          "zerolinecolor": {
            "label": "Zero Line Color",
            "default": "#333",
            "type": "color",
            "description": "The color of the line drawn at the 0 value."
          },

          "zerolinewidth": {
            "label": "Zero Line Width",
            "default": 1,
            "type": "int",
            "description": "The width of the line drawn at the 0 value."
          },

          "tickColor": {
            "label": "Tick Color",
            "default": "#333",
            "type": "color"
          },

          "minorTickColor": {
            "label": "Minor Tick Color",
            "default": "#383838",
            "type": "color"
          },

          "textColor": {
            "label": "Text Color",
            "default": "#ccc",
            "type": "color"
          },

          "range": {
            "label": "Range",

            "autorange": {
              "label": "Auto Range",
              "type": "boolean",
              "default": true,
              "description": "Whether or not the range of this axis is computed in relation to the input data. See 'Range Mode' for more info."
            },

            "rangemode": {
              "label": "Range Mode",
              "type": ["normal", "tozero", "nonnegative"],
              "default": "normal",
              "description": "**normal**: the range is computed in relation to the extrema of the input data.\n**tozero**: the range extends to 0 regardless of the input data.\n**nonnegative**: the range is non-negative, regardless of the input data."
            },

            "fixedrange": {
              "label": "Fixed Range",
              "type": "boolean",
              "default": false,
              "description": "Whether or not this axis is zoom-able. When true, zoom is disabled."
            },

            "start": {
              "label": "Start",
              "type": "float",
              "default": 0,
              "description": "The minimum value for the range of this axis."
            },

            "end": {
              "label": "End",
              "type": "float",
              "default": 0,
              "description": "The maximum value for the range of this axis."
            }
          },

          "grid": {
            "label": "Grid",

            "showgrid": {
              "label": "Show Grid",
              "default": false,
              "type": "boolean",
              "description": "Whether or not grid lines are drawn. If 'true' the grid lines are drawn at every tick mark."
            },

            "gridcolor": {
              "label": "Color",
              "default": "#eee",
              "type": "color",
              "description": "The color of the grid lines."
            },

            "gridwidth": {
              "label": "Width",
              "default": 1,
              "type": "int",
              "description": "The width of the grid lines (in pixels.)"
            }
          },

          "series": {
            "label": "Series Labels",
            "type": "array",
            "element": {
              "name": {
                "label": "Name",
                "type": "string",
                "default": "series"
              }
            }
          }
        },

        "yaxis": {
          "label": "Y Axis",

          "title": {
            "label": "Name",
            "default": "",
            "type": "string"
          },

          "type": {
            "label": "Type",
            "default": "detect",
            "type": ["detect", "linear", "log", "date", "category"],
            "description": "The axis type. By default, with 'detect', it attempts to determine the axis type by looking into the data of the groups that reference the axis in question."
          },

          "showline": {
            "label": "Show Line",
            "type": "boolean",
            "default": true,
            "description": "Whether or not the bounding line for the axis is drawn."
          },

          "linecolor": {
            "label": "Line Color",
            "default": "#222",
            "type": "color",
            "description": "The color of the axis line."
          },

          "linewidth": {
            "label": "Line Width",
            "default": 1,
            "type": "int",
            "description": "The width (in pixels) of the axis line."
          },

          "zeroline": {
            "label": "Zero Line",
            "default": true,
            "type": "boolean",
            "description": "Whether or not a line is drawn at the 0 value of the axis."
          },

          "zerolinecolor": {
            "label": "Zero Line Color",
            "default": "#333",
            "type": "color",
            "description": "The color of the line drawn at the 0 value."
          },

          "zerolinewidth": {
            "label": "Zero Line Width",
            "default": 1,
            "type": "int",
            "description": "The width of the line drawn at the 0 value."
          },

          "tickColor": {
            "label": "Tick Color",
            "default": "#333",
            "type": "color"
          },

          "minorTickColor": {
            "label": "Minor Tick Color",
            "default": "#383838",
            "type": "color"
          },

          "textColor": {
            "label": "Text Color",
            "default": "#ccc",
            "type": "color"
          },

          "range": {
            "label": "Range",

            "autorange": {
              "label": "Auto Range",
              "type": "boolean",
              "default": true,
              "description": "Whether or not the range of this axis is computed in relation to the input data. See 'Range Mode' for more info."
            },

            "rangemode": {
              "label": "Range Mode",
              "type": ["normal", "tozero", "nonnegative"],
              "default": "normal",
              "description": "**normal**: the range is computed in relation to the extrema of the input data.\n**tozero**: the range extends to 0 regardless of the input data.\n**nonnegative**: the range is non-negative, regardless of the input data."
            },

            "fixedrange": {
              "label": "Fixed Range",
              "type": "boolean",
              "default": false,
              "description": "Whether or not this axis is zoom-able. When true, zoom is disabled."
            },

            "start": {
              "label": "Start",
              "type": "float",
              "default": 0,
              "description": "The minimum value for the range of this axis."
            },

            "end": {
              "label": "End",
              "type": "float",
              "default": 0,
              "description": "The maximum value for the range of this axis."
            }
          },

          "grid": {
            "label": "Grid",

            "showgrid": {
              "label": "Show Grid",
              "default": false,
              "type": "boolean",
              "description": "Whether or not grid lines are drawn. If 'true' the grid lines are drawn at every tick mark."
            },

            "gridcolor": {
              "label": "Color",
              "default": "#eee",
              "type": "color",
              "description": "The color of the grid lines."
            },

            "gridwidth": {
              "label": "Width",
              "default": 1,
              "type": "int",
              "description": "The width of the grid lines (in pixels.)"
            }
          }
        }
      }
    }
  ]
}
